﻿using System;
using System.Text;
using MathBasic.Collections;
using MathBasic.Cryptography;
using MathBasic.Info;
using MathBasic.Net;

namespace ParameterSignture
{

    /// <summary>
    /// 此类提供 Web 参数签名；无法继承此类。
    /// </summary>
    public static class WebParameterSigntureUtility
    {
        /// <summary>
        ///  向指定的 URL 发送携带有 POST 数据的请求；该方法也同时实现了 GET 处理。
        /// </summary>
        /// <param name="url">要请求的 URL 路径，可以携带 GET 查询字符串。</param>
        /// <param name="postQueryString">请求携带的 POST 数据，其数据格式和 GET 查询字符串格式一致；如果该参数为 null 则发送 GET 请求，反之发送 POST 请求。</param>
        /// <param name="timeout">请求的有效时间。</param>
        /// <param name="encoding">请求需要使用的编码。</param>
        public static String RequestWebResult(String url, String postQueryString, Int32 timeout, Encoding encoding)
        {
            String result = null;
            RequestStatus status = RequestStatus.Failed;
            try {
                if (postQueryString != null) {
                    status = WebClient.SendRequestByPOST(new Uri(url), postQueryString, timeout, encoding, out result);
                }
                else {
                    status = WebClient.SendRequestByGET(new Uri(url), timeout, encoding, out result);
                }
            }
            catch {
            }
            switch (status) {
                case RequestStatus.Success:
                    return result;
                case RequestStatus.Timeout:
                    return "Timeout";
                default:
                    return "Failed";
            }
        }

    }

    /// <summary>
    /// 此类提供 Web 参数签名；无法继承此类。
    /// </summary>
    public sealed class WebParameterSignture
    {
        private Int64 m_Factor;
        private Encoding m_Encoding;
        private String m_Value, m_SeedKey;

        private static String GetValueEncoded(String value, Boolean get)
        {
            if (get) {
                return value.Replace('*', '+').Replace('.', '/').Replace('_', '=');
            }
            else {
                return value.Replace('+', '*').Replace('/', '.').Replace('=', '_');
            }
        }

        /// <summary>
        /// 初始化此类的新实例。
        /// </summary>
        /// <param name="factor">大于零的签名密钥索引。</param>
        /// <param name="encoding">参数的键值对的编码类型。</param>
        public WebParameterSignture(Int64 factor, Encoding encoding)
            : this(null, factor, encoding)
        {
        }

        /// <summary>
        /// 初始化此类的新实例。
        /// </summary>
        /// <param name="head">签名数据的默认头。</param>
        /// <param name="factor">大于零的签名密钥索引。</param>
        /// <param name="encoding">参数的键值对的编码类型。</param>
        public WebParameterSignture(String head, Int64 factor, Encoding encoding)
            : this(head, factor, null, encoding)
        {
        }

        /// <summary>
        /// 初始化此类的新实例。
        /// </summary>
        /// <param name="head">签名数据的默认头。</param>
        /// <param name="factor">大于零的签名密钥索引。</param>
        /// <param name="seedKey">要用于签名的密钥种子。</param>
        /// <param name="encoding">参数的键值对的编码类型。</param>
        public WebParameterSignture(String head, Int64 factor, String seedKey, Encoding encoding)
        {
            if (factor <= 0) {
                throw new ArgumentException("密钥索引必须大于零。");
            }
            else if (encoding == null) {
                throw new ArgumentException("编码不能为空引用。");
            }
            else {
                this.m_Factor = factor;
                this.m_SeedKey = seedKey;
                this.m_Value = head ?? String.Empty;
                this.m_Encoding = encoding;
            }
        }

        /// <summary>
        /// 添加键值对到签名容器中。
        /// </summary>
        /// <param name="key">键。</param>
        /// <param name="value">值。</param>
        public void Add(String key, String value)
        {
            if (value != null) {
                this.m_Value = KeyValueOptions.Insert(this.m_Value, this.m_Encoding, key, value, true, true);
            }
        }

        /// <summary>
        /// 从签名容器中移除指定的键值对。
        /// </summary>
        /// <param name="key">键。</param>
        /// <param name="value">值。</param>
        public void Remove(String key, String value)
        {
            this.m_Value = KeyValueOptions.Remove(this.m_Value, this.m_Encoding, key, value);
        }

        /// <summary>
        /// 获取签名容器中附带签名数据键值对集合的字符串表示形式。
        /// </summary>
        public override String ToString()
        {
            this.Add("_sign", WebParameterSignture.GetValueEncoded(Encryptor.Encrypt(this.m_Value, this.m_Factor, this.m_SeedKey), false));
            try {
                return this.m_Value;
            }
            finally {
                this.m_Value = String.Empty;
            }
        }

        /// <summary>
        /// 验证签名数据的正确性。
        /// </summary>
        /// <param name="factor">大于零的签名密钥索引。</param>
        /// <param name="encoding">参数的键值对的编码类型。</param>
        /// <param name="fromPOST">指示请求是否采用 POST 方式进行发送。</param>
        public static Boolean Verify(Int64 factor, Encoding encoding, Boolean fromPOST)
        {
            return WebParameterSignture.Verify(factor, null, encoding, fromPOST);
        }

        /// <summary>
        /// 验证签名数据的正确性。
        /// </summary>
        /// <param name="factor">大于零的签名密钥索引。</param>
        /// <param name="seedKey">要用于签名的密钥种子。</param>
        /// <param name="encoding">参数的键值对的编码类型。</param>
        /// <param name="fromPOST">指示请求是否采用 POST 方式进行发送。</param>
        public static Boolean Verify(Int64 factor, String seedKey, Encoding encoding, Boolean fromPOST)
        {
            if (factor > 0) {
                String queryString = Encryptor.Decrypt(WebParameterSignture.GetValueEncoded(fromPOST ? WebInfo.Form("_sign") : WebInfo.QueryString("_sign"), true), factor, seedKey);
                if (fromPOST) {
                    return WebInfo.SubFormParameter("_sign", null, encoding) == queryString;
                }
                else {
                    return WebInfo.SubHttpParameter("_sign", null, encoding) == queryString;
                }
            }
            return false;
        }

    }

}
